﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeFunc : MonoBehaviour
{
    [SerializeField] private GameObject treePart;
    [SerializeField] private GameObject log;
    [SerializeField] private GameObject wall;
    private GameObject createdTree;
    private GameObject createdWall;

    [SerializeField] Texture2D cursorTexture;
    public static CursorMode cursorMode = CursorMode.Auto;
    public static Vector2 hotSpot;
    public static bool chopped;
    private bool onTree;

    RaycastHit2D hit;

    void Start()
    { //set initial game objects
        onTree = false;
        treePart.SetActive(false);
        log.SetActive(false);
        wall.SetActive(true);
        createdTree = Instantiate(treePart);
        createdTree.transform.parent = Camera.main.transform;
        createdTree.transform.position = new Vector3(0, 0, 10);
        createdWall = Instantiate(wall);
        createdWall.transform.parent = Camera.main.transform;
        createdWall.transform.position = new Vector3(0, -4, 0);
        createdTree.SetActive(true);
        createdWall.SetActive(true);
    }

    void Update()
    {
        hit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Input.mousePosition));
 
        if (Input.GetMouseButtonDown(0) && onTree)
        { //run coroutine to execute, wait, execute
            StartCoroutine(Chop());
        }
        //change cursor when over tree ONLY
        if (hit.collider != null)
        {
            if (hit.collider.name.Equals("Tree(Clone)"))
            { 
                Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
                onTree = true;
            }
            else if (hit.collider.name.Equals("Wall(Clone)")) //still visible on tree and wall-------------------------
            {
                CursorMode cursorMode = CursorMode.Auto;
                Cursor.SetCursor(null, Vector2.zero, cursorMode);
                onTree = false;
            }
        }
        else {
            CursorMode cursorMode = CursorMode.Auto;
            Cursor.SetCursor(null, Vector2.zero, cursorMode);
            onTree = false;
        }
    }
    private IEnumerator Chop()
    {
        //make log
        createdTree.SetActive(false);
        Destroy(createdTree);
        createdTree = Instantiate(log);
        createdTree.transform.position = new Vector3(0, 0, 0);
        createdTree.SetActive(true);

        //5 sec delay to change back
        yield return new WaitForSeconds(5);

        //make tree again
        Destroy(createdTree);
        createdTree = Instantiate(treePart);
        createdTree.transform.position = new Vector3(0, 0, 10);
        createdTree.SetActive(true);

    }
}
